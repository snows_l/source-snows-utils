
简介：一些常用的自定义方法

**注意：请用最新版本（之前的有些版本有小问题，目前没有找到删除指定的版本的方法）
       v2.0.0之前的版本都是没有通过webpack打包的，文件相对于2.0.0之后的会大一点（在同等方法内）

一、安装以及使用

    1、安装：npm i snows-utils/yarn add snows-utils

    2、使用：

    1）：统一导入：import su from 'snows-utils'

    2）：分别导入：import { splitChunk,arr2tree, arr2percentage[, ...] } from 'snows-utils'

二、所有方法

 1、dataType

    1）、getDataType(param)
    2）、isStr(param)
    3）、isNum(param)
    4）、isArr(param)
    5）、isObj(param)
    6）、isFunc(param)
    7）、isNull(param)
    8）、isUndef(param)

  2、Vue2Directive  (目前都是vue2的自定义指令 基于vue2)

    1）、dialogDrag(directiveName)
    2）、dialogDragWidth(directiveName)
    3）、dialogDragHeight(directiveName)
    4）、dialogDragScale(directiveName)
    5）、dialogFullScreen(directiveName)
    6）、focus(directiveName)

  3、Url

    1）、isUrl(url)
    2）、getParamKeyByUrl(url, name)
    3）、getAllParamsByUrl(url)
    4）、addParamOnLocation(key, value)

  4、Array

    1）、arr2percentage(valueList,  digit)
    2）、splitChunk(source, singleNum)
    3）、arr2tree(source, id, parentId, children, isAddHasChildAttr, isDelNnllChildAttr)

  5、Number

    1）、num2chineseStr(number)
    2）、num2MoneyFormat(number)
    3）、num2micrometer(number)
    4）、removeMicromete(number)
    5）、randomNum(minNum, maxNum)
    6）、isDecimal(number)
    7）、decimalLen(number)
    8）、isMoney(param)

  6、String

    1）、transformEmail(email)
    2）、transformPhone(mobile)
    3）、str2num(str)
    4）、isEmail(param)
    5）、isPhone(param)

三、方法描述

  1、dataType

    1）、getDataType(param)
      /**
        * @description: 判断数据的类型
        * @param { * } param 需要判断类型的值
        * @return { * } 返回判断的类型
        */

    2）、isStr(param)
      /**
        * @description: 判断是否是 字符串 类型
        * @param { * } param
        * @return { Boolean }
        */

    3）、isNum(param)
      /**
        * @description: 判断是否是 数字 类型
        * @param { * } param
        * @return { Boolean }
        */

    4）、isArr(param)
      /**
        * @description: 判断是否是 数组 类型
        * @param { * } param
        * @return { Boolean }
        */

    5）、isObj(param)
      /**
        * @description: 判断是否是 对象 类型
        * @param { * } param
        * @return { Boolean }
        */

    6）、isFunc(param)
      /**
        * @description: 判断是否是 函数 类型
        * @param { * } param
        * @return { Boolean }
        */

    7）、isNull(param)
      /**
        * @description: 判断是否是 unll 类型
        * @param { * } param
        * @return { Boolean }
        */

    8）、isUndef(param)
      /**
        * @description: 判断是否是 undefined 类型
        * @param { * } param
        * @return { Boolean }
        */

  2、Vue2Directive  (目前都是vue2的自定义指令 基于vue2)

    1）、dialogDrag(directiveName = 'dialogDrag')
      /**
        * @description: 基于vue2和elmentUI的el-dialog弹框拖拽 自定义指令 (调用就可以直接使用)
        * @param { String } directiveName 指令名称（不用加 v- 前缀） 默认 v-dialogDrag
        */

    2）、dialogDragWidth(directiveName = 'dialogDragWidth')
      /**
        * @description: 基于vue2和elmentUI的el-dialog弹框宽度缩放 自定义指令 (调用就可以直接使用)
        * @param { String } directiveName 指令名称（不用加 v- 前缀） 默认 v-dialogDragWidth
        */

    3）、dialogDragHeight(directiveName = 'dialogDragHeight')
      /**
        * @description: 基于vue2和elmentUI的el-dialog弹框 高度缩放 自定义指令 (调用就可以直接使用)
        * @param { String } directiveName 指令名称（不用加 v- 前缀） 默认 v-dialogDragHeight
        */

    4）、dialogDragScale(directiveName = 'dialogDragScale')
      /**
        * @description: 基于vue2和elmentUI的el-dialog弹框 大小缩放 自定义指令 (调用就可以直接使用)
        * @param { String } directiveName 指令名称（不用加 v- 前缀） 默认 v-dialogDragScale
        */

    5）、dialogFullScreen(directiveName = 'dialogFullScreen')
      /**
        * @description: 基于vue2和elmentUI的el-dialog弹框 双击title实现全屏 自定义指令 (调用就可以直接使用)
        * @param { String } directiveName 指令名称（不用加 v- 前缀） 默认 v-dialogFullScreen
        */

    6）、focus(directiveName = 'focus')
      /**
        * @description: 基于vue2自动聚焦 自定义指令 (调用就可以直接使用)
        * @param { String } directiveName 指令名称（不用加 v- 前缀） 默认 v-focus
        */

  3、Url

    1）、isUrl(url)
      /**
        * @description: 判断是否是Url地址
        * @param { String } url url
        * @return { Boolean } 是否是url
        */

    2）、getParamKeyByUrl(url, name)
      /**
        * @description:  在地址栏中获取指定的参数值
        * @param { String } Url url地址
        * @param { String } name 获取那个参数的值
        * @return { String | null }
        */

    3）、getAllParamsByUrl(url)
      /**
        * @description: 在指定url中获取所有的参数
        * @param { String } url url地址
        * @return { Object } 返回params对象
        */

    4）、addParamOnLocation(key, value)
      /**
        * @description: 给当前url添加param
        * @param { String } key 添加的参数名
        * @param { String } value 参数值
        * @return { Null } 没有返回值
        */

  4、Array

    1）、arr2percentage(valueList,  digit)
        /**
          * @description: 数组自动计算百分之比，加起来等于100%
          * @param { Array } valueList 源数组
          * @param { Number } digit 保留几位小数 默认保留两位小数
          * @return { Array } 返回转成百分比的数组
          */

    2）、splitChunk(source, singleNum)
        /**
          * @description: 将源数组source,以每组singleNum个进行分成二位数组 (可以用作分页)
          * @param { Array } source 源数组
          * @param { Number } singleNum 每组各个数
          * @return { Array } 处理好的二维数组
          */

    3）、arr2tree(source, id, parentId, children, isAddHasChildAttr, isDelNnllChildAttr)
        /**
          * @description: 将数组结构的数据转成树形结构
          * @param { Array } source 需要转换的源数据
          * @param { String } id id字段 默认 'id'
          * @param { String } parentId 父节点字段 默认 'parentId'
          * @param { String } children 孩子节点字段 默认 'children'
          * @param { Boolean } isAddHasChildAttr 是否添加hasChild属性 默认 false
          * @param { Boolean } isDelNnllChildAttr 是否删除children为undefined的children属性 默认 false
          * @return { Array } 返回多棵树组成的数组
          */

  5、Number

    1）、num2chineseStr(number)
      /**
        * @description: 阿拉伯数字转换成汉字--普通
        * @param { Number } number
        * @return { Number }  转换之后的大写
        */

    2）、num2MoneyFormat(number)
      /**
        * @description: 阿拉伯数字转换金钱格式
        * @param { Number } number
        * @return { String } 转换之后的金额
        */

    3）、num2micrometer(number)
      /**
        * @description: 金额添加千分位
        * @param { Number } number 需要添加千分位的数
        * @return{ Number }
        */

    4）、removeMicromete(number)
      /**
        * @description: 去除千分位中的‘，’
        * @param { Number } number 带千分位的数字
        * @return { Number }
        */

    5）、randomNum(minNum, maxNum)
      /**
        * @description 生成从minNum到maxNum的随机数
        * @param { Number } minNum 最小值
        * @param { Number } maxNum 最大值
        * @return { Number } 随机数
        */

    6）、isDecimal(number)
      /**
        * @description: 判断是否是小数
        * @param{ Number }Number
        * @return{ Boolean }
        */

    7）、decimalLen(number)
      /**
        * @description: 检查一个小数的小数位长度
        * @param { Number } number
        * @return { Number }
        */

    8）、isMoney(param)
      /**
        * @description: 判断是否符合金额（保留两位小数）
        * @param { String } param 金额
        * @return { Boolean } 是否否符合金
        */

  6、String

    1）、transformEmail(email)
      /**
        * @description: 将邮箱地址转换成加密带*  eg：sno***l@163.com
        * @param {string} email 邮箱地址
        * @return {string} 转成之后的邮箱地址
        */

    2）、transformPhone(mobile)
      /**
        * @description: 将电话转换成加密带*的电话
        * @param { String } mobile 手机号码
        * @return { String } 返回加密的手机号码
        */

    3）、str2num(str)
      /**
        * @description: str转数字
        * @param { String } str
        * @return { Number }
        */

    4）、isEmail(param)
      /**
        * @description: 判断是否是邮箱地址
        * @param { String } param 地址
        * @return { Boolean } 是否是邮箱地址
        */

    5）、isPhone(param)
      /**
        * @description: 判断是否是手机号码
        * @param { String } param 手机号吗
        * @return { Boolean } 是否是手机号码
        */

  7、localStorage

    1）、local.get(key)
      /**
        * @description:获指定的本地存储
        * @param { string }  key 获取的key值
        * @retrun 返回获取到的数据
        */

    2）、local.set(key, value)
      /**
        * @description:  设置本地存储
        * @param {*} key 设置的key
        * @param {*} value 设置的值
        */

    3）、local.del(key)
      /**
        * @description: 删除本地存储的指定项
        * @param {*} key 删除的key
        */

    4）、local.clear()
      /**
        * @description: 清空本地存储
        */
