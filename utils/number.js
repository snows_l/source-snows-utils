/*
 * @Description: ------ 关于number的一些自定义方法 ------
 * @Creater: snows_l snows_l@163.com
 * @Date: 2022-12-17 12:56:35
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-06-30 15:51:10
 * @FilePath: /source-snows-utils/utils/number.js
 */
/* eslint-disable */
const Number = {
  /**
   * @description: 判断是否是小数
   * @param { Number } Number
   * @return { Boolean }
   */
  isDecimal(number) {
    if (typeof number == 'number') {
      return (number + '').indexOf('.') != -1;
    } else {
      return false;
    }
  },

  /**
   * @description: 检查一个小数的小数位长度
   * @param {*} number
   * @return {*}
   */
  decimalLen(number) {
    if (number instanceof Number) {
      if ((number + '').indexOf('.') != -1) {
        return (number + '').split('.')[1].length;
      } else {
        return 0;
      }
    } else {
      throw new Error(number + ' is not a Number');
    }
  },

  /**
   * @description: 阿拉伯数字转换成汉字--普通
   * @param {*} number
   * @return {string} 转换之后的大写
   */
  num2chineseStr(number) {
    if (!/^\d*(\.\d*)?$/.test(number)) {
      return 'Number is wrong!';
    }
    let AA = new Array('零', '一', '二', '三', '四', '五', '六', '七', '八', '九');
    let BB = new Array('', '十', '百', '千', '万', '亿', '点', '');
    let a = ('' + number).replace(/(^0*)/g, '').split('.'),
      k = 0,
      re = '';
    for (let i = a[0].length - 1; i >= 0; i--) {
      switch (k) {
        case 0:
          re = BB[7] + re;
          break;
        case 4:
          if (!new RegExp('0{4}\\d{' + (a[0].length - i - 1) + '}$').test(a[0])) re = BB[4] + re;
          break;
        case 8:
          re = BB[5] + re;
          BB[7] = BB[5];
          k = 0;
          break;
      }
      if (k % 4 == 2 && a[0].charAt(i + 2) != 0 && a[0].charAt(i + 1) == 0) re = AA[0] + re;
      if (a[0].charAt(i) != 0) re = AA[a[0].charAt(i)] + BB[k % 4] + re;
      k++;
    }
    if (a.length > 1) {
      //加上小数部分(如果有小数部分)
      re += BB[6];
      for (let i = 0; i < a[1].length; i++) re += AA[a[1].charAt(i)];
    }
    return re;
  },

  /**
   * @description: 阿拉伯数字转换金钱格式
   * @param {*} number
   * @return {*}
   */
  num2MoneyFormat(number) {
    if (!/^(0|[1-9]\d*)(\.\d+)?$/.test(number)) {
      return '数据非法'; //判断数据是否大于0
    }
    let unit = '仟佰拾亿仟佰拾万仟佰拾圆角分',
      str = '';
    number += '00';
    let indexpoint = number.indexOf('.'); // 如果是小数，截取小数点前面的位数
    if (indexpoint >= 0) {
      number = number.substring(0, indexpoint) + number.substr(indexpoint + 1, 2); // 若为小数，截取需要使用的unit单位
    }
    unit = unit.substr(unit.length - number.length); // 若为整数，截取需要使用的unit单位
    for (let i = 0; i < number.length; i++) {
      str += '零壹贰叁肆伍陆柒捌玖'.charAt(number.charAt(i)) + unit.charAt(i); //遍历转化为大写的数字
    }

    return str
      .replace(/零(仟|佰|拾|角)/g, '零')
      .replace(/(零)+/g, '零')
      .replace(/零(万|亿|圆)/g, '$1')
      .replace(/(亿)万|壹(拾)/g, '$1$2')
      .replace(/^圆零?|零分/g, '')
      .replace(/圆$/g, '圆整'); // 替换掉数字里面的零字符，得到结果
  },

  /**
   * @description: 金额添加千分位
   * @param {*} number
   * @return {*}
   */
  num2micrometer(number) {
    let numStr = String(Number(number).toFixed(0) - 0);
    if (isNaN(numStr)) {
      return number;
    }
    return numStr.replace(/\d(?=(?:\d{3})+\b)/g, '$&,');
  },

  /**
   * @description: 去除千分位中的‘，’
   * @param {*} number
   * @return {*}
   */
  removeMicromete(number) {
    if (!number) return number;
    number = number.toString();
    number = number.replace(/,/gi, '');
    return number;
  },

  /**
   * @description 生成从minNum到maxNum的随机数
   * @param { Number } minNum 最小值
   * @param { Number } maxNum 最大值
   * @return { Number } 随机数
   */
  randomNum(minNum, maxNum) {
    switch (arguments.length) {
      case 1:
        return parseInt(Math.random() * minNum + 1, 10);
      case 2:
        return parseInt(Math.random() * (maxNum - minNum + 1) + minNum, 10);
      default:
        return 0;
    }
  },

  /**
   * @description: 判断是否符合金额（保留两位小数）
   * @param { String } param 金额
   * @return { Boolean } 是否否符合金
   */
  isMoney(param) {
    return /^(([1-9]{1}\d*)|(0{1}))(\.\d{1,2})?$/.test(param);
  }
};

export default Number;
