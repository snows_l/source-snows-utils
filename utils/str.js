/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-06-24 17:42:55
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-08-26 16:36:16
 * @FilePath: /source-snows-utils/utils/str.js
 */

const str = {
  /**
   * @description: 将邮箱地址转换成加密带*  eg：sno***l@163.com
   * @param {string} email 邮箱地址
   * @return {string} 转成之后的邮箱地址
   */
  transformEmail(email) {
    let newEmail = '';
    if (String(email).indexOf('@') > 0) {
      const str = email.split('@');
      let _s = '';
      if (str[0].length > 4) {
        for (let i = 0; i < str[0].length - 4; i++) {
          _s += '*';
        }
      }
      newEmail = str[0].substr(0, 3) + _s + str[0].substr(str[0].length - 1) + '@' + str[1];
    }
    return newEmail;
  },

  /**
   * @description: 将电话转换成加密带*的电话
   * @param {*} mobile 手机号码
   * @return {*} 返回加密的手机号码
   */
  transformPhone(mobile) {
    let newMobile = '';
    if (mobile.length > 7) {
      newMobile = mobile.substr(0, 3) + '****' + mobile.substr(7);
    }
    return newMobile;
  },

  str2num(str) {
    let num = '';
    for (let i = 0; i < str.length; ++i) {
      num += str[i].charCodeAt() - 65;
    }
    return num;
  },

  /**
   * @description: 判断是否是邮箱地址
   * @param { String } param 地址
   * @return { Boolean } 是否是邮箱地址
   */
  isEmail(param) {
    return /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/.test(param);
  },

  /**
   * @description: 判断是否是手机号码
   * @param { String } param 手机号吗
   * @return { Boolean } 是否是手机号码
   */
  isPhone(param) {
    // return /^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|166|198|199|(147))\d{8}$/.test(param);
    return /^(1[3-9])\d{9}$/.test(param);
  }
};

export default str;
