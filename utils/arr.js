/*
 * @Description: ------------ 关于数组的自定义方法 -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-06-24 17:33:47
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-06-28 00:45:38
 * @FilePath: /source-snows-utils/utils/arr.js
 */
const arr = {
  /**
   * @description: 将源数组source,以每组singleNum个进行分成二位数组 (可以用作分页)
   * @param { Array } source 源数组
   * @param { Number } singleNum 每组各个数
   * @return { Array } 处理好的二维数组
   */
  splitChunk(source, singleNum) {
    if (Object.prototype.toString.call(source).slice(8, -1) === 'Array' && Object.prototype.toString.call(parseInt(singleNum)).slice(8, -1) === 'Number') {
      return source.reduce((total, item, index) => {
        const total_index = parseInt(index / parseInt(singleNum));
        if (!total[total_index]) total[total_index] = [];
        total[total_index].push(item);
        return total;
      }, []);
    } else {
      return [];
    }
  },

  /**
   * @description: 将数组结构的数据转成树形结构
   * @param { Array } source 需要转换的源数据
   * @param { String } id id字段 默认 'id'
   * @param { String } parentId 父节点字段 默认 'parentId'
   * @param { String } children 孩子节点字段 默认 'children'
   * @param { Boolean } isAddHasChildAttr 是否添加hasChild属性 默认 false
   * @param { Boolean } isDelNnllChildAttr 是否删除children为undefined的children属性 默认 false
   * @return { Array } 返回多棵树组成的数组
   */
  arr2tree(source, id, parentId, children, isAddHasChildAttr, isDelNnllChildAttr) {
    let config = {
      id: id || 'id',
      parentId: parentId || 'parentId',
      childrenList: children || 'children',
      isAddHasChildAttr: isAddHasChildAttr || false,
      isDelNnllChildAttr: isDelNnllChildAttr || false
    };

    var childrenListMap = {};
    var nodeIds = {};
    var tree = [];

    for (let d of source) {
      let parentId = d[config.parentId];
      if (childrenListMap[parentId] == null) {
        childrenListMap[parentId] = [];
      }
      nodeIds[d[config.id]] = d;
      childrenListMap[parentId].push(d);
    }

    for (let d of source) {
      let parentId = d[config.parentId];
      if (nodeIds[parentId] == null) {
        tree.push(d);
      }
    }

    for (let t of tree) {
      adaptToChildrenList(t);
    }

    function adaptToChildrenList(o) {
      if (childrenListMap[o[config.id]] !== null) {
        o[config.childrenList] = childrenListMap[o[config.id]];
      }
      if (o[config.childrenList]) {
        if (config.isAddHasChildAttr) o['hasChild'] = true;
        for (let c of o[config.childrenList]) {
          adaptToChildrenList(c);
        }
      } else {
        if (config.isAddHasChildAttr) o['hasChild'] = false;
        if (config.isDelNnllChildAttr) delete o[config.childrenList];
      }
    }

    function setLevel(t, level) {
      t.__level__ = level;
      if (!t.children || !t.children.length) return;
      t.children.forEach(t => setLevel(t, level + 1));
    }
    tree.forEach(t => setLevel(t, 0));

    return tree;
  },

  /**
   * @description: 数组自动计算百分之比，加起来等于100%
   * @param { Array } valueList 源数组
   * @param { Number } digit 保留几位小数
   * @return { Array } 返回转成百分比的数组
   */
  arr2percentage(valueList, digit = 2) {
    if (Object.prototype.toString.call(valueList).slice(8, -1) !== 'Array') throw new Error(`${valueList} is not a Array`);
    let sum = valueList.reduce(function (acc, val) {
      return acc + (isNaN(val) ? 0 : val);
    }, 0);
    if (sum === 0) {
      return 0;
    }
    // sum 9
    let digits = Math.pow(10, digit); // digits 100
    let votesPerQuota = valueList.map(function (val) {
      return ((isNaN(val) ? 0 : val) / sum) * digits * 100; // 扩大比例，这样可以确保整数部分是已经确定的议席配额，小数部分是余额
    });
    // votesPerQuota [ 2222.222222222222, 4444.444444444444, 3333.333333333333 ] 每一个项获得的议席配额，整数部分是已经确定的议席配额，小数部分是余额
    let targetSeats = digits * 100; // targetSeats 10000 全部的议席
    let seats = votesPerQuota.map(function (votes) {
      return Math.floor(votes);
    });
    let currentSum = seats.reduce(function (acc, val) {
      return acc + val;
    }, 0);
    let remainder = votesPerQuota.map(function (votes, idx) {
      return votes - seats[idx];
    });
    while (currentSum < targetSeats) {
      let max = Number.NEGATIVE_INFINITY;
      let maxId = null;
      for (let i = 0, len = remainder.length; i < len; ++i) {
        if (remainder[i] > max) {
          max = remainder[i];
          maxId = i;
        }
      }
      ++seats[maxId]; // 第二项，即4的占比的坐席增加1
      remainder[maxId] = 0;
      ++currentSum; // 总的已分配的坐席数也加1
    }
    return seats.map(item => {
      return item / digits;
    });
  }
};

export default arr;
