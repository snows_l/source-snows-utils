/*
 * @Description: ------ 文件描述 ------
 * @Creater: snows_l snows_l@163.com
 * @Date: 2022-12-18 17:53:40
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-06-24 19:44:38
 * @FilePath: /xTools/utils/url.js
 */
/* eslint-disable */
const Url = {
  /**
   * @description: 判断是否是Url地址
   * @param { String } url url
   * @return { Boolean } 是否是url
   */
  isUrl(url) {
    return /^(http|https):\/\/([\w.]+\/?)\S*/.test(url);
  },

  /**
   * @description:  在地址栏中获取指定的参数值
   * @param { String } Url url地址
   * @param { String } name 获取那个参数的值
   * @return { String | null }
   */
  getParamKeyByUrl(url, name) {
    var reg = new RegExp(`(^|&)${name}=([^&]*)(&|$)`);
    // 匹配目标参数
    let r = null;
    if (url.indexOf('?') != -1) {
      r = url.split('?')[1].match(reg);
    }
    if (r != null) return r[2];
    // 返回参数值
    return null;
  },

  /**
   * @description: 在指定url中获取所有的参数
   * @param { String } url url地址
   * @return { Object } 返回params对象
   */
  getAllParamsByUrl(url) {
    let params = {};
    if (Object.prototype.toString.call(url).slice(8, -1) != 'String') {
      return params;
    }
    let paramsStr = url.split('?')[1];
    if (!paramsStr) {
      return params;
    }
    let paramsArr = paramsStr.split('&');
    paramsArr.forEach(item => {
      if (item.split('=')[0]) params[item.split('=')[0]] = item.split('=')[1];
    });
    return params;
  },

  /**
   * @description: 给当前url添加param
   * @param { String } key 添加的参数名
   * @param { String } value 参数值
   * @return { Null } 没有返回值
   */
  addParamOnLocation(key, value) {
    // 调用 ==> 获取到添加参数后的地址
    var newurl = updateQueryStringParameter(window.location.href, key, value);
    //向当前url添加参数，没有历史记录
    window.history.replaceState(
      {
        path: newurl
      },
      '',
      newurl
    );
    /**
     * @description: 给指定url添加param,并返回添加之后的url
     * @param { String } url 要添加的url
     * @param { String } key 添加的参数名
     * @param { String | Number } value 参数值
     * @return { String } 返回添加之后的url
     */
    function updateQueryStringParameter(url, key, value) {
      if (!value) {
        return url;
      }
      var re = new RegExp('([?&])' + key + '=.*?(&|$)', 'i');
      var separator = url.indexOf('?') !== -1 ? '&' : '?';
      if (url.match(re)) {
        return url.replace(re, '$1' + key + '=' + value + '$2');
      } else {
        return url + separator + key + '=' + value;
      }
    }
  }
};

export default Url;
