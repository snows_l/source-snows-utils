/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-06-24 17:24:57
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-06-25 21:03:12
 * @FilePath: /xTools/utils/index.js
 */
import { isNum } from './type';
const other = {
  /**
   * @description: 判断是否是闰年
   * @param { Number} year 判断的年份
   * @return { Boolean } 返回是否是闰年
   */
  isLeapyear(year) {
    if (isNum(year * 1)) {
      if ((year % 100 != 0 && year % 4 == 0) || year % 400 == 0) {
        return true;
      } else {
        return false;
      }
    } else {
      throw new Error(`${year} 不是一个年份 `);
    }
  },

  /**
   * @description: 去除html标签，只留下纯文本内容
   * @param { String } str 要去除的html文档
   * @return { String } 返回纯文本数据
   */
  removeHTMLAndAtrr(str) {
    if (!str) return '';
    return str.replace(/<\/?.+?>/g, '').replace(/ /g, '');
  },

  /**
   * @description: 深拷贝
   * @param { Object | Array } obj  要拷贝的源数据
   * @return { Object | Array } 拷贝之后的数据
   */
  deepClone(obj) {
    // 先检测是不是数组和Object
    // let isArr = Object.prototype.toString.call(obj) === '[object Array]';
    let isArr = Array.isArray(obj);
    let isJson = Object.prototype.toString.call(obj) === '[object Object]';
    if (isArr) {
      // 克隆数组
      let newObj = [];
      for (let i = 0; i < obj.length; i++) {
        newObj[i] = deepClone(obj[i]);
      }
      return newObj;
    } else if (isJson) {
      // 克隆Object
      let newObj = {};
      for (let i in obj) {
        newObj[i] = deepClone(obj[i]);
      }
      return newObj;
    }
    // 不是引用类型直接返回
    return obj;
  },

  /**
   * @description: 防抖函数
   * @param {*} fn 回调函数
   * @param {*} delay 防抖时间
   * @return {*}
   */
  debounce(fn, delay = 500) {
    // timer 是在闭包中的
    let timer = null;
    return function () {
      if (timer) {
        clearTimeout(timer);
      }
      timer = setTimeout(() => {
        fn.apply(this, arguments);
        timer = null;
      }, delay);
    };
  },

  /**
   * @description: 节流
   * @param { Function } fn 回调函数
   * @param { number } delay 要节流的时间
   * @return {*}
   */
  // 节流
  throttle(fn, delay = 500) {
    let timer;
    let prevTime;
    return function (...args) {
      let currTime = Date.now();
      let context = this;
      if (!prevTime) prevTime = currTime;
      clearTimeout(timer);

      if (currTime - prevTime > delay) {
        prevTime = currTime;
        fn.apply(context, args);
        clearTimeout(timer);
        return;
      }
      timer = setTimeout(function () {
        prevTime = Date.now();
        timer = null;
        fn.apply(context, args);
      }, delay);
    };
  }
};

export default other;
