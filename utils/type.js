/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-06-24 17:36:40
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-06-24 22:55:54
 * @FilePath: /xTools/utils/type.js
 */
// 判断数据是那种类型
const getDataType = unknown => Object.prototype.toString.call(unknown).slice(8, -1);

/**
 * @description 自定义判断是否是某种类型
 */
const isNull = unknown => getDataType(unknown) === 'Null';
const isUndef = unknown => getDataType(unknown) === 'Undefined';
const isStr = unknown => getDataType(unknown) === 'String';
const isArr = unknown => getDataType(unknown) === 'Array';
const isObj = unknown => getDataType(unknown) === 'Object';
const isNum = unknown => getDataType(unknown) === 'Number';
const isFunc = unknown => getDataType(unknown) === 'Function';

export { getDataType, isArr, isFunc, isNull, isNum, isObj, isStr, isUndef };

export default {
  getDataType,
  isStr,
  isNum,
  isArr,
  isObj,
  isFunc,
  isNull,
  isUndef
};
