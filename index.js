/*
 * @Description: ------ 入口文件 进行统一整理暴露 ------
 * @Creater: snows_l snows_l@163.com
 * @Date: 2022-12-18 16:57:54
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-08-28 17:47:50
 * @FilePath: /source-snows-utils/index.js
 */
import Arr from './utils/arr';
import Other from './utils/index';
import Local from './utils/local';
import Num from './utils/number';
import Str from './utils/str';
import Type from './utils/type';
import Url from './utils/url';
import vue2Directive from './utils/vue2Directive';

// arr
export const splitChunk = Arr.splitChunk;
export const arr2tree = Arr.arr2tree;
export const arr2percentage = Arr.arr2percentage;

// number
export const isDecimal = Num.isDecimal;
export const decimalLen = Num.decimalLen;
export const num2chineseStr = Num.num2chineseStr;
export const num2MoneyFormat = Num.num2MoneyFormat;
export const num2micrometer = Num.num2micrometer;
export const removeMicromete = Num.removeMicromete;
export const randomNum = Num.randomNum;

// str
export const transformEmail = Str.transformEmail;
export const transformPhone = Str.transformPhone;
export const str2num = Str.str2num;

// type
export const getDataType = Type.getDataType;
export const isArr = Type.isArr;
export const isFunc = Type.isFunc;
export const isNull = Type.isNull;
export const isNum = Type.isNum;
export const isObj = Type.isObj;
export const isStr = Type.isStr;
export const isUndef = Type.isUndef;

// url
export const isUrl = Url.isUrl;
export const getParamKeyByUrl = Url.getParamKeyByUrl;
export const getAllParamsByUrl = Url.getAllParamsByUrl;
export const addParamOnLocation = Url.addParamOnLocation;

// Other
export const isLeapyear = Other.isLeapyear;
export const removeHTMLAndAtrr = Other.removeHTMLAndAtrr;
export const deepClone = Other.deepClone;
export const debounce = Other.debounce;
export const throttle = Other.throttle;

//  vue2Directive
export const dialogDrag = vue2Directive.dialogDrag;
export const dialogDragWidth = vue2Directive.dialogDragWidth;
export const dialogDragHeight = vue2Directive.dialogDragHeight;
export const dialogDragScale = vue2Directive.dialogDragScale;
export const dialogFullScreen = vue2Directive.dialogFullScreen;
export const focus = vue2Directive.focus;

// localStorage
export const local = Local;

const all = { ...Arr, ...Num, ...Str, ...Type, ...Url, ...Other, ...vue2Directive, Local };
export default all;
