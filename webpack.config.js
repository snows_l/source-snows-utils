/*
 * @Description: ------------ snows-utils webpack 打包配置 -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-06-26 01:27:49
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-06-26 16:51:04
 * @FilePath: /source-snows-utils/webpack.config.js
 */

const path = require('path');

module.exports = {
  mode: 'production',
  entry: './index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'index.mini.js',
    libraryTarget: 'umd' // 输入的文件格式  umd：全兼容模式
  },
  // 忽略没有引用的第三方模块
  externals: {
    vue: 'vue' // 忽略没有引用的vue
  },

  optimization: {
    usedExports: false // 关闭副作用标识功能
  }
};
